# Implementing basic algorithm on Scala. 

The objective of this lab is to solve computational problems, using basic Scala constructions as:
1. Variable definitions, conditionals, loops.
2. Scala Objects, function and methods.
3. Test using features of ```scalatest``` package.

Also to get familiar with Intellij IDE.     
   

## Order 

Given the ```Lab2``` scala ```Object``` definition [Lab2.scala](src/main/scala/session2/Lab2.scala)

1. Implement the following using class ```Lab2Test``` ([Lab2Test.scala](src/test/scala/session2/Lab2Test.scala)) as tester:
    - Given an array of elements calculate the sum
    - Given and array of elements calculate the max
    - Given and array of elements calculate the min
    - Given and array of elements calculate the diff between max and min
    - Given and array of "Double" elements calculate the average

## Intellij

- To run test:
    - Right click on ```src->scala->session2->Lab2Test``` pick the option ```Run Lab2Test```
    - Green means test passed, red mean test failure. 
